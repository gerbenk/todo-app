import { Component } from '@angular/core';
import { AuthService } from './service/auth.service';
import { ReplaySubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Grocery list';
  authenticated$: ReplaySubject<boolean>;

  constructor(public authService: AuthService, public router: Router) {
    this.authenticated$ = this.authService.authenticated$;
  }

  logout() {
    this.authService.logout();
  }

}

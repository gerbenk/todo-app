import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReplaySubject, of } from 'rxjs';
import { FingerprintService } from './fingerprint.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl: string = environment.api_url;
  fingerprintUrl: string = environment.fingerprint_url;
  authenticated$: ReplaySubject<boolean>;
  isAuthenticated: boolean;
  headers: HttpHeaders;

  constructor(public httpClient: HttpClient, public fingerprintService: FingerprintService) {
    this.authenticated$ = new ReplaySubject<boolean>(1);
    this.authenticated$.next(false);
    this.headers = new HttpHeaders();
  }

  setIsAuthenticated(bool: boolean) {
    this.isAuthenticated = bool;
    console.log('setIsAuthenticated');
    this.authenticated$.next(this.isAuthenticated);
  }

  isAuthenticated$(): ReplaySubject<boolean> {
    return this.authenticated$;
  }

  login(username: string, password: string) {
    // to do: connect to API
    return this.httpClient.post(`${this.apiUrl}/login`, {username, password}).subscribe(
      resp => this.setIsAuthenticated(true)
    );
  }

  logout() {
    this.setIsAuthenticated(false);
  }

  register(username: string, password: string) {

    /*
    1. Genereer een UUID aan de hand van je die unieke browser ID.
    2. registreer die met POST /register/{id}
    3. Gebruik deze id in alle calls door hem mee te sturen als X-Token

    OF met een echte account

    1. registreer je met username en password
    POST /register
    {
      "username": "hans",
      "password": "hans"
    }
    2. Login
    POST /login
    {
      "username": "hans",
      "password": "hans"
    }

    3. In de response headers vind je een veld "Authorization"
    4. gebruik die waarde bij alle nieuwe requests als header "Authorization"
    */

    return this.httpClient.post<any>(`${this.apiUrl}/register`, {username, password})
      .subscribe( (resp) => {
        this.headers.append('Authorization', resp.Authorization);
        this.setIsAuthenticated(true);
      });
  }

  registerDevice() {
    /*
    1. Genereer een UUID aan de hand van je die unieke browser ID.
    2. registreer die met POST /register/{id}
    3. Gebruik deze id in alle calls door hem mee te sturen als X-Token
    */
    console.log('mock authservice.registerDevice');
    // get fingerprint from external API
    const fingerprint = this.fingerprintService.getFingerprint().then(
      (resp) => {
        // set fingerprint as X-Token in headers
        console.log('fingerprint', fingerprint);
        this.headers = this.headers.set('X-Token', resp);
        console.log('headers', this.headers);
        console.log('headers X-Token', this.headers.get('X-Token'));
        this.setIsAuthenticated(true);
      }
    );

    // return this.httpClient.post(`${this.apiUrl}/register/${fingerprint.fingerprint}`, {}).subscribe(
    //   (resp) => console.log(resp)
    // );
  }

}

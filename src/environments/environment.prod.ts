export const environment = {
  production: true,
  api_url: 'https://groceries.kegel.it',
  fingerprint_url: 'http://www.devpowerapi.com/fingerprint',
};

import { TestBed } from '@angular/core/testing';

import { FingerprintService } from './fingerprint.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FingerprintService', () => {
  let service: FingerprintService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(FingerprintService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

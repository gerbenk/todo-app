import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Task, List } from '../models/task';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  apiUrl: string;

  constructor(public httpClient: HttpClient, private authService: AuthService) {
    this.apiUrl = environment.api_url;
  }

  getList$(): Observable<List[]> {
    return this.httpClient.get<List[]>(`${this.apiUrl}/lists`, { headers: this.authService.headers });
  }

  getList(listId: string): Observable<List> {
    return this.getList$().pipe(
      map(txs => txs.find(l => l.id === listId))
    );
  }

  addList(list: List) {
    return this.httpClient.post<List>(`${this.apiUrl}/lists`, list, { headers: this.authService.headers });
  }

  deleteList(list: List) {
    console.log('deleteList', list);
    return this.httpClient.delete<List>(`${this.apiUrl}/lists/${list.id}`, { headers: this.authService.headers });
  }

  getTask$(listId: string): Observable<Task[]> {
    console.log('TodoService.getTask$ headers', this.authService.headers.get('X-Token'));
    return this.httpClient.get<Task[]>(`${this.apiUrl}/lists/${listId}/tasks`, { headers: this.authService.headers });
  }

  createTask(task: Task, listId: string): Observable<Task> {
    return this.httpClient.post<Task>(`${this.apiUrl}/lists/${listId}/tasks/`, task, { headers: this.authService.headers });
  }

  deleteTask(task: Task, listId: string): Observable<object>  {
    console.log('service.delete', task);
    return this.httpClient.delete(`${this.apiUrl}/lists/${listId}/tasks/${task.id}`, { headers: this.authService.headers });
  }

  checkTask(task: Task, listId: string): Observable<object> {
    return this.httpClient.post(`${this.apiUrl}/lists/${listId}/tasks/${task.id}/check`, task, { headers: this.authService.headers });
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { TodoService } from '../service/todo.service';
import { Observable } from 'rxjs';
import { List, Task } from '../models/task';
import { NewTaskComponent } from '../new-task/new-task.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})

export class TaskListComponent implements OnInit {

  @ViewChild('newTaskComponent') newTaskComponent: NewTaskComponent;

  id: string;
  list: List;
  task$: Observable<Task[]>;
  tasks: Task[];
  lists: any;

  constructor(private service: TodoService, private activatedroute: ActivatedRoute) {
    this.activatedroute.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.fetchList();
      this.tasks = [];
      this.fetchTasks();
    });

  }

  ngOnInit(): void {
  }

  fetchList() {
    this.service.getList(this.id).subscribe(
      resp => this.list = resp
    );
  }

  fetchTasks() {
    this.task$ = this.service.getTask$(this.id);
    this.task$.subscribe(
      resp => {
        this.tasks = resp;
        console.log('this.lists', this.lists);
      }
    );
  }

  deleteTask(task: Task) {
    // pessimistic delete: only delete task after receiving okay from service
    this.service.deleteTask(task, this.id).subscribe(
      () => {
        this.tasks = this.tasks.filter(t => {
          return t.id !== task.id;
        });
      }
    );
  }

  addTask(task: Task) {
    // optimistic creation: add task before receiving okay from service
    // todo: get task_id from service and update task
    this.tasks.push(task);
    this.service.createTask(task, this.id).subscribe(
      resp => {
        task.id = resp.id;
        this.newTaskComponent.resetForm();
      }
    );
  }

  checkTask(task: Task) {
    task.checked = true;
    this.service.checkTask(task, this.id).subscribe();
  }

}

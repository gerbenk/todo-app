import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Task } from '../../models/task';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  apiUrl: string;
  task$: Observable<Task[]>;
  tasks: Task[];

  constructor(private authService: AuthService) {
    const task1 = new Task();
    task1.id = 'task1';
    task1.name = 'Aardappels';
    const task2 = new Task();
    task2.id = 'task2';
    task2.name = 'Bloemkool';
    this.tasks = [task1, task2];
    this.task$ = of(this.tasks);
  }

  getTasks(): Observable<Task[]> {
    return this.task$;
  }

  create(task: Task): Observable<Task> {
    this.tasks.push(task);
    return of(task);
  }

  delete(task: Task): Observable<object>  {
    console.log('service.delete', task);
    this.tasks = this.tasks.filter(
      (t) => t.id !== task.id
    );
    return of({message: 'ok'});
  }

  check(task: Task): Observable<object> {
    task.checked = true;
    return of(task);
  }

}

import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // listen to changes in authentication status
    this.auth.authenticated$.subscribe(
      authenticated => {
        if (!authenticated) {
          this.router.navigate(['login']);
        }
      }
    );
    if (!this.auth.isAuthenticated) {
      console.log('authguard redirecting to /login');
      console.log('state.url', state.url);
      return false;
    }
    return true;
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Task } from '../models/task';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit {

  @Output() add: EventEmitter<object> = new EventEmitter();
  newTaskForm: FormGroup;

  constructor( private formBuilder: FormBuilder ) {
    this.newTaskForm = this.formBuilder.group({
      name: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    // Process checkout data here
    if (this.newTaskForm.value.name.length > 0) {
      this.add.emit(this.newTaskForm.value);
    }
  }

  resetForm() {
    this.newTaskForm.setValue({
      name: ''
    });
  }
}

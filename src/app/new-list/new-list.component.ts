import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.scss']
})
export class NewListComponent implements OnInit {

  @Output() add: EventEmitter<object> = new EventEmitter();
  newListForm: FormGroup;
  busy = false;

  constructor( private formBuilder: FormBuilder ) {
    this.newListForm = this.formBuilder.group({
      name: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    // Process checkout data here
    if (this.newListForm.value.name.length > 0) {
      this.busy = true;
      this.add.emit(this.newListForm.value);
    }
  }

  resetForm() {
    this.newListForm.setValue({
      name: ''
    });
    this.busy = false;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { LoginComponent } from './login.component';
import { AuthService } from '../service/auth.service';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let spy: jasmine.Spy;
  let de: DebugElement;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        RouterTestingModule,
        FormsModule,
        HttpClientTestingModule,
        MatCardModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatToolbarModule
      ],
      providers: [AuthService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
    authService = de.injector.get(AuthService);
    spy = spyOn(authService, 'login').and.returnValue(null);
  });

  it('username should not be present after init', () => {
    expect(component.username).toBeUndefined();
  });

  it('should be 2 input fields present', () => {
    expect(de.queryAll(By.css('input')).length).toBe(2);
  });

  it('should be 1 text input present', () => {
    console.log(de.queryAll(By.css('input[type=text]')).length);
    expect(de.queryAll(By.css('input[type=text]')).length).toBe(1);
  });

  it('should connect authService.login after login attempt', () => {
    component.username = 'dummy';
    component.password = 'fake';
    component.login();
    expect(spy).toHaveBeenCalled();
    expect(spy.calls.all().length).toEqual(1);
  });

});

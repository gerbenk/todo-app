import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  returnUrl: string;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    if (this.authService.isAuthenticated) {
      this.router.navigate(['/']);
    }
    this.authService.authenticated$.subscribe(
      data => {
        if (data) {
          this.router.navigate([this.returnUrl]);
        }
      });
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    console.log('this.returnUrl', this.returnUrl);
  }

  login() {
    // todo: form validation
    this.authService.login(this.username, this.password);
  }

  register() {
    // todo: form validation
    console.log( 'register', this.username, this.password);
    this.authService.register(this.username, this.password).subscribe( (resp) => {
      console.log('registerd', resp);
      this.login();
    });
  }

  registerDevice() {
    this.authService.registerDevice();
  }

}

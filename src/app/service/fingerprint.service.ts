import { Injectable } from '@angular/core';
declare var Fingerprint2: any;

@Injectable({
  providedIn: 'root'
})
export class FingerprintService {

  fingerprint: string;

  constructor() {
    Fingerprint2.get( (components) => {
      const values = components.map( (component) => component.value );
      const murmur: string = Fingerprint2.x64hash128(values.join(''), 31);
      // tslint:disable-next-line: max-line-length
      this.fingerprint = `${murmur.substr(0, 8)}-${murmur.substr(8, 4)}-${murmur.substr(12, 4)}-${murmur.substr(16, 4)}-${murmur.substr(20, 12)}`;
    });
  }

  public async getFingerprint(): Promise<string> {
    return new Promise((resolve, reject) => {
      resolve(this.fingerprint);
    });
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../models/task';
import { trigger, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition('* => swipeleft', [
        animate('200ms ease-in', style({transform: 'translateX(-100%)'}))
      ]),
      transition('* => swiperight', [
        animate('200ms ease-in', style({transform: 'translateX(100%)'}))
      ]),
      transition(':enter', [
        style({transform: 'translateY(200%)'}),
        animate('100ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
    ])
  ]
})
export class TaskComponent implements OnInit {
  @Output() delete: EventEmitter<Task> = new EventEmitter();
  @Output() check: EventEmitter<Task> = new EventEmitter();
  @Input() task: Task;
  animationState = '';

  constructor() { }

  ngOnInit(): void {
  }

  swipeleft() {
    this.animationState = 'swipeleft';
    this.delete.emit();
  }

  swiperight() {
    this.animationState = 'swiperight';
    this.delete.emit();
  }

  press() {
    console.log('pressed');
  }

  tap() {
    console.log('tapped');
  }

  resetAnimationState() {
    this.animationState = '';
  }

}

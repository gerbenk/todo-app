import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReplaySubject, of } from 'rxjs';
import { FingerprintService } from './fingerprint.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl: string = environment.api_url;
  authenticated$: ReplaySubject<boolean>;
  isAuthenticated: boolean;
  headers: HttpHeaders;

  constructor(public httpClient: HttpClient, public fingerprintService: FingerprintService) {
    this.authenticated$ = new ReplaySubject<boolean>(1);
    this.authenticated$.next(false);
    this.headers = new HttpHeaders();

    // check for stored authentication info in LocalStorage
    if (localStorage.getItem('X-Token')) {
      console.log('localStorage has item X-Token', localStorage.getItem('X-Token'));
      this.headers = this.headers.set('X-Token', localStorage.getItem('X-Token'));
      this.setIsAuthenticated(true);
    } else if (localStorage.getItem('Authorization')) {
      console.log('localStorage has item Authorization', localStorage.getItem('Authorization'));
      this.headers = this.headers.set('Authorization', localStorage.getItem('Authorization'));
      this.setIsAuthenticated(true);
    }
  }

  setIsAuthenticated(bool: boolean) {
    console.log('authenticated?', bool);
    this.isAuthenticated = bool;
    this.authenticated$.next(this.isAuthenticated);
  }

  isAuthenticated$(): ReplaySubject<boolean> {
    return this.authenticated$;
  }

  login(username: string, password: string) {
    // to do: connect to API
    return this.httpClient.post(`${this.apiUrl}/login`, {username, password}).subscribe(
      resp => this.setIsAuthenticated(true)
    );
  }

  logout() {
    localStorage.clear();
    this.headers = new HttpHeaders();
    this.setIsAuthenticated(false);
  }

  register(username: string, password: string) {

    /*
    1. Genereer een UUID aan de hand van je die unieke browser ID.
    2. registreer die met POST /register/{id}
    3. Gebruik deze id in alle calls door hem mee te sturen als X-Token

    OF met een echte account

    1. registreer je met username en password
    POST /register
    {
      "username": "hans",
      "password": "hans"
    }
    2. Login
    POST /login
    {
      "username": "hans",
      "password": "hans"
    }

    3. In de response headers vind je een veld "Authorization"
    4. gebruik die waarde bij alle nieuwe requests als header "Authorization"
    */

    return this.httpClient.post<any>(`${this.apiUrl}/register`, {username, password});
  }

  registerDevice() {
    /*
    1. Genereer een UUID aan de hand van je die unieke browser ID.
    2. registreer die met POST /register/{id}
    3. Gebruik deze id in alle calls door hem mee te sturen als X-Token
    */

    // get fingerprint from external API
    this.fingerprintService.getFingerprint().then(
      (fingerprint) => {
        this.httpClient.post(`${this.apiUrl}/register/${fingerprint}`, {}).subscribe(
          (resp) => {
            // set fingerprint as X-Token in headers
            console.log('device registered', resp);
            console.log('X-Token = ', fingerprint);
            this.headers = this.headers.set('X-Token', fingerprint);
            localStorage.setItem('X-Token', fingerprint);
            this.setIsAuthenticated(true);
          }
        );
      }
    );

  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { List } from '../models/task';
import { TodoService } from '../service/todo.service';
import { Animations } from '../animations/animations';
import { trigger, style, transition, animate } from '@angular/animations';
import { NewListComponent } from '../new-list/new-list.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-lists',
  templateUrl: './task-lists.component.html',
  styleUrls: ['./task-lists.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition('* => swipeleft', [
        animate('200ms ease-in', style({transform: 'translateX(-100%)'}))
      ]),
      transition('* => swiperight', [
        animate('200ms ease-in', style({transform: 'translateX(100%)'}))
      ]),
      transition(':enter', [
        style({transform: 'translateY(200%)'}),
        animate('100ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
    ])
  ]
})
export class TaskListsComponent implements OnInit {

  @ViewChild('newListComponent') newListComponent: NewListComponent;
  lists: List[];
  animationState = '';
  busyAdding = false;

  constructor(private service: TodoService, public router: Router) {
    this.fetchLists();
  }

  ngOnInit(): void {
  }

  fetchLists() {
    this.service.getList$().subscribe(
      resp => {
        this.lists = resp;
        console.log('this.lists', this.lists);
      }
    );
  }

  addList(list: List) {
    this.busyAdding = true;
    this.service.addList(list).subscribe(
      resp => {
        this.lists.push(resp);
        this.newListComponent.resetForm();
      }
    );
  }

  deleteList(list: List) {
    this.service.deleteList(list).subscribe(
      resp => this.lists = this.lists.filter(
        l => l.id !== list.id
      )
    );
  }

  swipeleft(list: List) {
    this.animationState = 'swipeleft';
    this.deleteList(list);
  }

  swiperight(list: List) {
    this.animationState = 'swiperight';
    this.deleteList(list);
  }

  press(list: List) {
    console.log('pressed');
    this.router.navigate([`lists/${list.id}`]);
  }

  tap(list: List) {
    console.log('tapped');
    this.router.navigate([`lists/${list.id}`]);
  }

  resetAnimationState() {
    this.animationState = '';
  }


}

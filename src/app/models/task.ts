export class Task {
    id: string;
    name: string;
    checked: boolean;

    constructor() {
        this.checked = false;
    }
}

export class List {
    id: string;
    name: string;
    tasks?: Task[];

    constructor() {
    }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FingerprintService {

  constructor() { }

  public async getFingerprint(): Promise<string> {
    const hardcodedUuid = '70366714-142c-47ba-bf31-57824bfb2d57';
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(hardcodedUuid);
      }, 10);
    });
  }
}

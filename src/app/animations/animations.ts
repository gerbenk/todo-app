import { trigger, style, transition, animate } from '@angular/animations';

export const Animations = {
    animeTrigger: trigger('slideInOut', [
        transition('* => swipeleft', [
            animate('200ms ease-in', style({transform: 'translateX(-100%)'}))
        ]),
        transition('* => swiperight', [
            animate('200ms ease-in', style({transform: 'translateX(100%)'}))
        ]),
        transition(':enter', [
            style({transform: 'translateY(200%)'}),
            animate('100ms ease-in', style({transform: 'translateY(0%)'}))
        ]),
    ])
};

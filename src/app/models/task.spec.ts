
import { Task } from './task';

describe('Task', () => {
  it('should create an instance', () => {
    expect(new Task()).toBeTruthy();
  });
  it('should set checked to false on a new instance', () => {
    expect(new Task().checked).toBeFalse();
  });
});
